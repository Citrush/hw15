# dont edit this function
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# my script
name="Dalton Witt"
now=$(date +%r)

arg_array=($*)


if [ $# -eq 0 ]; 
then
	usage
else
	
	echo $name
	date
	echo
	
	for arg in "${arg_array[@]}"; do
		case "$arg" in
		"TestError")
			usage "TestError found"
			echo "*****"
		;;
		"now")
			echo "It is now $now"
			echo "*****"
		;;
		*)
			usage "Do not know what to do with $arg"
			echo "*****"
		;;
		esac
	done
fi
